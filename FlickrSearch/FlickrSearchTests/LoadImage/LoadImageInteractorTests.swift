//
//  LoadImageInteractorTests.swift
//  FlickrSearchTests
//
//  Created by Puneet Sharma2 on 02/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import XCTest
@testable import FlickrSearch

class LoadImageInteractorTests: XCTestCase {
    var interactor:LoadImageInteractor!
    var queue:DispatchQueue!
    var service:LoadImageTestService!
    
    override func setUp() {
        super.setUp()
        queue = DispatchQueue(label: "test.queue")
        service = LoadImageTestService()
        interactor = LoadImageInteractor.init(service: service, queue:queue)
    }
    
    override func tearDown() {
        super.tearDown()
        queue = nil
        service = nil
        interactor = nil
    }
    
    func testInteractorIsMakingRequest() {
        let request = LoadImageInteractor.Request.init(id: "id", secret: "secret", farm: 3, server: "server")
        interactor.getImage(request: request) { response in
            
        }
        queue.sync {}
        // Test Request
        XCTAssertNotNil(service.request)
        XCTAssertTrue(service.request!.imagePath.contains("id"))
        XCTAssertTrue(service.request!.imagePath.contains("secret"))
        XCTAssertTrue(service.request!.imagePath.contains("server"))
        XCTAssertTrue(service.request!.imagePath.contains("3"))
    }
    
    func testInteractorIsMakingResponse() {
        let request = LoadImageInteractor.Request.init(id: "id", secret: "secret", farm: 3, server: "server")
        service.createErrorResponse = true
        let errorExpectation = expectation(description: "error_response")
        var errorResponse:LoadImageInteractor.Response?
        interactor.getImage(request: request) { response in
            errorResponse = response
            errorExpectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNil(errorResponse!.image)
        XCTAssertNotNil(errorResponse!.error)
        XCTAssertTrue(errorResponse!.error! == LoadImageInteractor.Error.noImageFound)
        
        service.createErrorResponse = false
        let successExpectation = expectation(description: "success_response")
        var successResponse:LoadImageInteractor.Response?
        interactor.getImage(request: request) { response in
           successResponse = response
            successExpectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNil(successResponse!.error)
        XCTAssertNotNil(successResponse!.image)
    }
}
