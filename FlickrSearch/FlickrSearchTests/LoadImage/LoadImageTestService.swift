//
//  LoadImageTestService.swift
//  FlickrSearchTests
//
//  Created by Puneet Sharma2 on 02/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation
import UIKit
@testable import FlickrSearch

class LoadImageTestService:LoadImageService {
    public var request:LoadImage.Request?
    public var createErrorResponse:Bool = false
    
    func getImageWith(_ request: LoadImage.Request, completion: @escaping (LoadImage.Response) -> ()) {
        self.request = request
        if createErrorResponse {
            completion(LoadImage.Response.init(path: request.imagePath, image: nil, error: LoadImageInteractor.Error.noImageFound))
        } else {
            completion(LoadImage.Response.init(path: request.imagePath, image: UIImage.init(), error: nil))
        }
    }
}
