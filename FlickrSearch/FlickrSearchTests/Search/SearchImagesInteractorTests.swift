//
//  SearchImagesInteractorTests.swift
//  FlickrSearchTests
//
//  Created by Puneet Sharma2 on 01/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import XCTest
@testable import FlickrSearch

class SearchImagesInteractorTests: XCTestCase {
    var interactor:SearchImagesInteractor!
    var queue:DispatchQueue!
    var service:SearchImagesTestService!
    
    override func setUp() {
        super.setUp()
        queue = DispatchQueue(label: "test.queue")
        service = SearchImagesTestService()
        interactor = SearchImagesInteractor.init(service: service, queue:queue)
    }
    
    override func tearDown() {
        super.tearDown()
        queue = nil
        service = nil
        interactor = nil
    }
    
    func testInteractorIsMakingRequest() {
        let request:SearchImagesInteractor.Request = SearchImagesInteractor.Request.init(text: "kitten", page: 1, perPage: 100)
        interactor.getSearchResult(request: request) { response in
            
        }
        queue.sync {}
        // Test Request
        XCTAssertNotNil(service.request)
        XCTAssertTrue(service.request!.text == "kitten")
        XCTAssertTrue(service.request!.page == 1)
        XCTAssertTrue(service.request!.perPage == 100)
    }
    
    func testInteractorIsMakingResponse() {
        let request:SearchImagesInteractor.Request = SearchImagesInteractor.Request.init(text: "kitten", page: 1, perPage: 100)
        var errorResponse:SearchImagesInteractor.Response?
        let errorExpectation = expectation(description: "error_response")
        service.createErrorResponse = true
        
        interactor.getSearchResult(request: request) { response in
            errorResponse = response
            errorExpectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNil(errorResponse!.searchResponse)
        
        service.createErrorResponse = false
        var successResponse:SearchImagesInteractor.Response?
        let successExpectation = expectation(description: "success_response")
        interactor.getSearchResult(request: request) { response in
            successResponse = response
            successExpectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(successResponse!.searchResponse)
    }
}
