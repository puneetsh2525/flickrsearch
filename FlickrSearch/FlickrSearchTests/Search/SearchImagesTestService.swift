//
//  SearchImagesTestService.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 01/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation
@testable import FlickrSearch

final class SearchImagesTestService:SearchImagesService {
    public var request:SearchService.Request?
    public var createErrorResponse:Bool = false
    
    lazy var testData:Data = {
        let jsonString:String =
        """
            {"photos":{"page":1,"pages":254879,"perpage":2,"total":"509757","photo":[{"id":"29467969067","owner":"151187872@N05","secret":"3212267dbb","server":"1877","farm":2,"title":"DSC05880","ispublic":1,"isfriend":0,"isfamily":0},{"id":"43687897194","owner":"161310557@N08","secret":"f42f1a4260","server":"1853","farm":    2,"title":"20180825_0195","ispublic":1,"isfriend":0,"isfamily":0}]},"stat":"ok"}
        """
        return jsonString.data(using: .utf8)!
    }()
    
    public func getImagesWith(_ request: SearchService.Request, completion:@escaping (SearchService.Response) -> ()) {
        self.request = request
        if createErrorResponse {
            completion(SearchService.Response.init(search: nil, error: nil))
        } else {
            do {
                let search = try Photos.Search.decode(data: testData)
                completion(SearchService.Response.init(search: search, error: nil))
            }
            catch (let error) {
                print(error)
                   completion(SearchService.Response.init(search: nil, error: error))
            }
        }
    }
}
