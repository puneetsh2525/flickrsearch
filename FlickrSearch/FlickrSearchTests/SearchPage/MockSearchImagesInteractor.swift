//
//  MockSearchImagesInteractor.swift
//  FlickrSearchTests
//
//  Created by Puneet Sharma2 on 02/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation
@testable import FlickrSearch

class MockSearchImagesInteractor:SearchImagesInteractor {
    public var request:SearchImagesInteractor.Request?
    
    public override func getSearchResult(request:SearchImagesInteractor.Request, completion:@escaping (SearchImagesInteractor.Response)->()) {
        self.request = request
    }
}
