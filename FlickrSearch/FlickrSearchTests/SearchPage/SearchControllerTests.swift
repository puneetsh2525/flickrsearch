//
//  SearchControllerTests.swift
//  FlickrSearchTests
//
//  Created by Puneet Sharma2 on 02/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import XCTest
@testable import FlickrSearch

class SearchControllerTests: XCTestCase {
    var searchController:SearchController!
    var searchInteractor:MockSearchImagesInteractor!
    var loadInteractor:MockLoadImageInteractor!
    
    override func setUp() {
        super.setUp()
        searchInteractor = MockSearchImagesInteractor()
        loadInteractor = MockLoadImageInteractor()
        searchController = SearchController.init(searchInteractor!, loadImageInteractor: loadInteractor!)
    }
    
    override func tearDown() {
        searchInteractor = nil
        loadInteractor = nil
        searchController = nil
        super.tearDown()
    }
    
    func testSearchViewIsLoaded() {
        let view = searchController!.view!
        XCTAssertTrue(view is SearchView)
    }
    
    func testDidPressSearchText() {
        var text = ""
        searchController.didPressSearchWithText(text)
        XCTAssertNil(searchInteractor.request)
        
        text = "kitten"
        searchController.didPressSearchWithText(text)
        XCTAssertNotNil(searchInteractor.request)
        XCTAssertTrue(searchController.viewModel.currentSearchText! == "kitten")
    }
    
    func testDidReachEndOfScroll() {
        var text = ""
        searchController.didReachEndOfScroll()
        XCTAssertNil(searchInteractor.request)
        
        text = "kitten"
        searchController.didPressSearchWithText(text)
        
        searchController.didReachEndOfScroll()
        XCTAssertNotNil(searchInteractor.request)
        
        XCTAssertTrue(searchController.viewModel.currentPage+1 == searchInteractor.request?.page ?? -100)
        XCTAssertTrue((searchInteractor.request?.text ?? "test") == "kitten")
    }
    
    func testWillShowImageView() {
        searchController.willShowImageViewAt(IndexPath.init(row: 10, section: 0))
        XCTAssertNil(loadInteractor.request)
        
        let items = [SearchViewModel.Image].init(repeating: SearchViewModel.Image.init(id: "id", secret: "secret", farm: 1, server: "server"), count: 10)
        searchController.viewModel.setItems(items)
        
        searchController.willShowImageViewAt(IndexPath.init(row: 10, section: 0))
        XCTAssertNil(loadInteractor.request)
        
        searchController.willShowImageViewAt(IndexPath.init(row: 9, section: 0))
        XCTAssertNotNil(loadInteractor.request)
        
        XCTAssertTrue(loadInteractor.request!.farm == 1)
        XCTAssertTrue(loadInteractor.request!.id == "id")
        XCTAssertTrue(loadInteractor.request!.secret == "secret")
        XCTAssertTrue(loadInteractor.request!.server == "server")
    }
}
