//
//  SearchPresenterTests.swift
//  FlickrSearchTests
//
//  Created by Puneet Sharma2 on 02/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import XCTest
@testable import FlickrSearch

class SearchPresenterTests: XCTestCase {
    var viewModel:SearchViewModel!
    var presenter:SearchPresenter!
    
    override func setUp() {
        super.setUp()
        viewModel = SearchViewModel()
        presenter = SearchPresenter()
    }
    
    override func tearDown() {
        super.tearDown()
        viewModel = nil
        presenter = nil
    }
    
    func testData(page:String) -> Data {
        let jsonString:String =
        """
            {"photos":{"page":\(page),"pages":254879,"perpage":2,"total":"509757","photo":[{"id":"29467969067","owner":"151187872@N05","secret":"3212267dbb","server":"1877","farm":2,"title":"DSC05880","ispublic":1,"isfriend":0,"isfamily":0},{"id":"43687897194","owner":"161310557@N08","secret":"f42f1a4260","server":"1853","farm":    2,"title":"20180825_0195","ispublic":1,"isfriend":0,"isfamily":0}]},"stat":"ok"}
        """
        return jsonString.data(using: .utf8)!
    }
    
    func testShowLoader() {
        var isShowLoaderCalled = false
        viewModel.showLoader = {
            isShowLoaderCalled = true
        }
        _ = presenter.subscribeSearchResponse(viewModel: viewModel)
        XCTAssertTrue(isShowLoaderCalled)
    }
    
    func testHideLoader() {
        var isHideLoaderCalled = false
        viewModel.hideLoader = {
            isHideLoaderCalled = true
        }
        var response = presenter.subscribeSearchResponse(viewModel: viewModel)
        response(SearchImagesInteractor.Response.init(searchText: "kitten", searchResponse: nil, error: SearchImagesInteractor.Error.noDataFromNetwork))
        
        XCTAssertTrue(isHideLoaderCalled)
        
        isHideLoaderCalled = false
        response = presenter.subscribeSearchResponse(viewModel: viewModel)
        response(SearchImagesInteractor.Response.init(searchText: "kitten", searchResponse: nil, error: SearchImagesInteractor.Error.inProgress))
        XCTAssertFalse(isHideLoaderCalled)
    }
    
    func testScrollToTop() {
        var isScrollToTopCalled = false
        viewModel.scrollToTop = {
            isScrollToTopCalled = true
        }
        viewModel.setCurrentSearchText("kitten")
        let response = presenter.subscribeSearchResponse(viewModel: viewModel)
        response(SearchImagesInteractor.Response.init(searchText: "kitten", searchResponse: nil, error: SearchImagesInteractor.Error.noDataFromNetwork))
        XCTAssertFalse(isScrollToTopCalled)
        
        var search = try? Photos.Search.decode(data: testData(page: "1"))
        response(SearchImagesInteractor.Response.init(searchText: "kitten", searchResponse: search, error: nil))
        XCTAssertTrue(isScrollToTopCalled)
        
        isScrollToTopCalled = false
        search = try? Photos.Search.decode(data: testData(page: "2"))
        response(SearchImagesInteractor.Response.init(searchText: "kitten", searchResponse: search, error: nil))
        XCTAssertFalse(isScrollToTopCalled)
    }
    
    func testLoadItems() {
        
        let response = presenter.subscribeSearchResponse(viewModel: viewModel)
        
        var isLoadCalled = false
        viewModel.loadItems = {
            isLoadCalled = true
        }
        viewModel.setCurrentSearchText("kitten")
        var search = try? Photos.Search.decode(data: testData(page: "1"))
        response(SearchImagesInteractor.Response.init(searchText: "kitten", searchResponse: search, error: nil))
        
        XCTAssertTrue(isLoadCalled)
        XCTAssertTrue(viewModel.items.count == 2)
        
        isLoadCalled = false
        search = try? Photos.Search.decode(data: testData(page: "2"))
        response(SearchImagesInteractor.Response.init(searchText: "kitten", searchResponse: search, error: nil))
        XCTAssertTrue(isLoadCalled)
        XCTAssertTrue(viewModel.items.count == 4)
    }
}
