//
//  MockLoadImageInteractor.swift
//  FlickrSearchTests
//
//  Created by Puneet Sharma2 on 02/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation
@testable import FlickrSearch

class MockLoadImageInteractor:LoadImageInteractor {
    var request:LoadImageInteractor.Request?
    public override func getImage(request:LoadImageInteractor.Request, completion:@escaping (LoadImageInteractor.Response) -> ()) {
        self.request = request
    }
}
