//
//  SearchImagesInteractor.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 01/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation

class SearchImagesInteractor {
    private let searchService:SearchImagesService
    private var textPageMap:[Int:Bool] = [:]
    private let serialQueue:DispatchQueue
    
    required init(service:SearchImagesService, queue:DispatchQueue = DispatchQueue(label: "com.SearchImagesInteractor.serial.queue")) {
        self.searchService = service
        self.serialQueue = queue
    }
    
    convenience init() {
        self.init(service: SearchImagesNetworkService())
    }
    
    public func getSearchResult(request:SearchImagesInteractor.Request, completion:@escaping (SearchImagesInteractor.Response)->()) {
        serialQueue.async {
            guard self.textPageMap[request.hashValue] == nil else {
                completion(Response.init(searchText:request.text, searchResponse: nil, error: .inProgress))
                return
            }
            
            self.textPageMap[request.hashValue] = true
            let searchImagesRequest = SearchService.Request.init(text: request.text, page: request.page, perPage: request.perPage)
            self.searchService.getImagesWith(searchImagesRequest) {[weak self] response in
                self?.handle(response, request: request, completion: completion)
            }
        }
        
    }
    
    private func handle(_ response:SearchService.Response, request:SearchImagesInteractor.Request, completion:@escaping (SearchImagesInteractor.Response) -> ()) {
        serialQueue.async {
            let error = response.search == nil ? SearchImagesInteractor.Error.noDataFromNetwork : nil
            completion(Response.init(searchText:request.text, searchResponse: response.search, error: error))
            self.textPageMap[request.hashValue] = nil
        }
    }
}

extension SearchImagesInteractor {
    enum Error:Swift.Error {
        case noDataFromNetwork
        case inProgress
    }
    
    struct Request:Hashable {
        let text:String
        let page:Int
        let perPage:Int
    }
    
    struct Response {
        let searchText:String
        let searchResponse:Photos.Search?
        let error:Error?
    }
}
