//
//  LoadImageInteractor.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 01/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation
import UIKit

class LoadImageInteractor {
    private let service:LoadImageService
    private var idMap:[String:String] = [:]
    private let serialQueue:DispatchQueue
    
    required init(service:LoadImageService, queue:DispatchQueue = DispatchQueue(label: "com.LoadImageInteractor.serial.queue")) {
        self.service = service
        self.serialQueue = queue
    }
    
    convenience init() {
        self.init(service: LoadImageNetworkService())
    }
    
    public func getImage(request:LoadImageInteractor.Request, completion:@escaping (LoadImageInteractor.Response) -> ()) {
        serialQueue.async {
            let path = FlickrAPI.image(LoadImage.ImagePathRequest(id: request.id, secret: request.secret, farm: request.farm, server: request.server)).urlString
            
            guard self.idMap[path] == nil else {
                completion(LoadImageInteractor.Response.init(id: self.idMap[path]!, image: nil, error: .inProgress))
                return
            }
            
            self.idMap[path] = request.id
            let request = LoadImage.Request.init(imagePath:path)
            self.service.getImageWith(request) {[weak self] response in
                self?.handle(response, completion: completion)
            }
        }
    }
    
    private func handle(_ response:LoadImage.Response, completion:@escaping (LoadImageInteractor.Response) -> ()) {
        serialQueue.async {
            let error = response.image == nil ? LoadImageInteractor.Error.noImageFound : nil
            let interactorResponse = LoadImageInteractor.Response.init(id: self.idMap[response.path]!, image: response.image, error: error)
            completion(interactorResponse)
            self.idMap[response.path] = nil
        }
    }
}

extension LoadImageInteractor {
    enum Error:Swift.Error {
        case noImageFound
        case inProgress
    }
    
    struct Request {
        let id:String
        let secret:String
        let farm:Int
        let server:String
    }
    
    struct Response {
        let id:String
        let image:UIImage?
        let error:Error?
    }
}
