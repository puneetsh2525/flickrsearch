//
//  Decodable+Additions.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 01/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation

extension Decodable {
    public static func decode(data:Data) throws -> Self {
        let jsonDecoder = JSONDecoder()
        return try jsonDecoder.decode(Self.self, from: data)
    }
}
