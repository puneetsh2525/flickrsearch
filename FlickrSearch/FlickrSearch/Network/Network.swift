//
//  Network.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 01/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation

enum FlickrAPI {
    case search(SearchService.Request)
    case image(LoadImage.ImagePathRequest)
}

extension FlickrAPI {
    var urlRequest:URLRequest {
        switch self {
        case .search(let params):
            var urlComponents = URLComponents.init(string: urlString)!
            var queryParams:[URLQueryItem] = [URLQueryItem.init(name: "method", value: "flickr.photos.search")]
            queryParams.append(URLQueryItem.init(name: "api_key", value: "3e7cc266ae2b0e0d78e279ce8e361736"))
            queryParams.append(URLQueryItem.init(name: "format", value: "json"))
            queryParams.append(URLQueryItem.init(name: "nojsoncallback", value: "1"))
            queryParams.append(URLQueryItem.init(name: "safe_search", value: "1"))
            queryParams.append(URLQueryItem.init(name: "text", value: params.text))
            queryParams.append(URLQueryItem.init(name: "page", value: params.page.description))
            queryParams.append(URLQueryItem.init(name: "per_page", value: params.perPage.description))
            urlComponents.queryItems = queryParams
            return URLRequest.init(url: urlComponents.url!)
            
        case .image(_) :
            return URLRequest.init(url: URL.init(string: urlString)!)
        }
    }
}

extension FlickrAPI {
    var urlString:String {
        switch self {
        case .search(_):
            return "https://api.flickr.com/services/rest/"
        case .image(let params):
            var string = "http://farm{farm}.static.flickr.com/{server}/{id}_{secret}.jpg"
            string = string.replacingOccurrences(of: "{farm}", with: params.farm.description)
            string = string.replacingOccurrences(of: "{server}", with: params.server)
            string = string.replacingOccurrences(of: "{id}", with: params.id)
            string = string.replacingOccurrences(of: "{secret}", with: params.secret)
            return string
        }
    }
}
