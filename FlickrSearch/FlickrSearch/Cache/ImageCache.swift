//
//  ImageCache.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 01/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation
import UIKit

class ImageCache {
    static let shared:ImageCache = ImageCache.init()
    private let memoryCache:NSCache<NSString, UIImage> = NSCache()
    private let concurrentQueue:DispatchQueue =  DispatchQueue.init(label: "ImageCache.concurrent.queue", qos: DispatchQoS.default, attributes: DispatchQueue.Attributes.concurrent, autoreleaseFrequency: DispatchQueue.AutoreleaseFrequency.inherit, target: nil)
    private let diskCache = FileManager.default.urls(for: FileManager.SearchPathDirectory.cachesDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first!
    
    private init() {
    }
    
    public func getImage(_ path:String, completion:@escaping (UIImage?) -> ()) {
        let newPath = path.replacingOccurrences(of: "/", with: "")
        concurrentQueue.async {[unowned self] in
            let image = self.memoryCache.object(forKey: newPath as NSString)
            guard image == nil else {
                completion(image)
                return
            }
            guard let imageFromDisk = self.getImageFromDisk(newPath) else {
                completion(nil)
                return
            }
            self.setImageInMemory(imageFromDisk, path: newPath)
            completion(imageFromDisk)
        }
    }
    
    public func setImage(_ image:UIImage, path:String) {
        let newPath = path.replacingOccurrences(of: "/", with: "")
        setImageInMemory(image, path: newPath)
        setImageInDisk(image, path: newPath)
    }
    
    private func setImageInMemory(_ image:UIImage, path:String) {
        concurrentQueue.async(flags: .barrier) {
            self.memoryCache.setObject(image, forKey: path as NSString)
        }
    }
    
    private func setImageInDisk(_ image:UIImage, path:String) {
        concurrentQueue.async(flags: .barrier) {
            let diskPath = self.diskCache.appendingPathComponent(path, isDirectory: false)
            guard FileManager.default.fileExists(atPath: diskPath.absoluteString) == false else {return}
            guard let data = UIImageJPEGRepresentation(image, 1.0) else {return}
            do{
                try  data.write(to: diskPath)
            } catch(let error) {
                print(error)
            }
        }
    }
    
    private func getImageFromDisk(_ path:String) -> UIImage? {
        let diskPath = diskCache.appendingPathComponent(path, isDirectory: false)
        let data = try? Data.init(contentsOf: diskPath)
        return data != nil ? UIImage.init(data: data!) : nil
    }
}
