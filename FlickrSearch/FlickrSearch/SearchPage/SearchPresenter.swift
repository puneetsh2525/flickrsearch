//
//  SearchPresenter.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 02/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation

final class SearchPresenter {
}


// MARK:- Search
extension SearchPresenter {
    public func subscribeSearchResponse(viewModel:SearchViewModel) -> ((SearchImagesInteractor.Response) -> ()) {
        viewModel.showLoader?()
        let response:((SearchImagesInteractor.Response) -> ()) = {[weak self]response in
            self?.handle(response: response, viewModel: viewModel)
        }
        return response
    }
    
    private func handle(response:SearchImagesInteractor.Response, viewModel:SearchViewModel) {
        handleLoaderHide(response: response, viewModel: viewModel)
        guard viewModel.currentSearchText == response.searchText else {return}
        guard let search = response.searchResponse else {return}
        let images = search.photos.photo.map{SearchViewModel.Image(id: $0.id, secret: $0.secret, farm: $0.farm, server: $0.server)}
        viewModel.setCurrentPage(search.photos.page)
        if search.photos.page == 1 {
            viewModel.setItems(images)
            viewModel.scrollToTop?()
        } else {
            var newItems = viewModel.items
            newItems += images
            viewModel.setItems(newItems)
        }
    }
    
    private func handleLoaderHide(response:SearchImagesInteractor.Response,  viewModel:SearchViewModel) {
        if let error = response.error, error == .inProgress {
            // do nothing
        } else {
             viewModel.hideLoader?()
        }
    }
}

// MARK:- LoadImages
extension SearchPresenter {
    public func subscribeImageLoadResponse(viewModel:SearchViewModel) -> ((LoadImageInteractor.Response)->()) {
        let response:((LoadImageInteractor.Response)->()) = { response in
            guard let image = response.image, let index = viewModel.items.index(where: {$0.id == response.id}) else {return}
            viewModel.setImage(image, forIndex: index)
        }
        return response
    }
}
