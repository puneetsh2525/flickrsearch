//
//  SearchViewModel.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 02/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation
import UIKit

final class SearchViewModel {
    struct Image {
        let id:String
        let secret:String
        let farm:Int
        let server:String
    }
    
    public private(set) var items:[SearchViewModel.Image] = [] {
        didSet {
            self.loadItems?()
        }
    }
    
    public private(set) var currentPage:Int = 0
    public private(set) var currentSearchText:String?
    
    public var showListError:(()->())?
    public var scrollToTop:(()->())?
    public var showLoader:(()->())?
    public var hideLoader:(()->())?
    public var loadItems:(() -> ())?
    public var loadImageAtIndex:((UIImage, Int) -> ())?
    
    public func setCurrentPage(_ page:Int) {
        self.currentPage = page
    }
    
    public func setCurrentSearchText(_ text:String) {
        self.currentSearchText = text
    }
    
    public func setItems(_ items:[SearchViewModel.Image]) {
        self.items = items
    }
    
    public func setImage(_ image:UIImage, forIndex index:Int) {
        self.loadImageAtIndex?(image, index)
    }
 
}
