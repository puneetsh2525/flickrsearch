//
//  SearchView.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 02/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation
import UIKit

final class SearchView:UIView {
    
    let inset: CGFloat = 10
    let minimumLineSpacing: CGFloat = 10
    let minimumInteritemSpacing: CGFloat = 10
    let cellsPerRow = 3
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var collectionView:UICollectionView! {
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.keyboardDismissMode = .onDrag
            if #available(iOS 11.0, *) {
                collectionView.contentInsetAdjustmentBehavior = .always
            }
            collectionView.register(UINib.init(nibName: SearchResultCollectionViewCell.name(), bundle: nil), forCellWithReuseIdentifier: SearchResultCollectionViewCell.name())
        }
    }
    
    @IBOutlet var searchBar:UISearchBar! {
        didSet {
            searchBar.delegate = self
            searchBar.showsCancelButton = true
        }
    }
    
    private let activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    private unowned let controller:SearchController
    
    public init(controller:SearchController) {
        self.controller = controller
        super.init(frame: .zero)
        Bundle.main.loadNibNamed("SearchView", owner: self, options: nil)
        addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|[v]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: ["v":contentView]
            )
        )
        self.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:|[v]|",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil, views: ["v":contentView]
            )
        )
        addIndicatorView()
        setUpViewModel()
    }
    
    public func addIndicatorView() {
        activityIndicator.hidesWhenStopped = true
        searchBar.addSubview(activityIndicator)
    }
    
    public func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK:- UICollectionViewDataSource, UICollectionViewDelegate
extension SearchView:UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return controller.viewModel.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchResultCollectionViewCell.name(), for: indexPath) as! SearchResultCollectionViewCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        (cell as! SearchResultCollectionViewCell).configure(image: nil)
        controller.willShowImageViewAt(indexPath)
        
        if indexPath.row+1 == controller.viewModel.items.count {
            // last row
            controller.didReachEndOfScroll()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var leftInset:CGFloat = 0
        var rightInset:CGFloat = 0
        if #available(iOS 11.0, *) {
            leftInset = collectionView.safeAreaInsets.left
            rightInset = collectionView.safeAreaInsets.right
        }
        let marginsAndInsets = inset * 2 + leftInset + rightInset + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((self.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        return CGSize(width: itemWidth, height: itemWidth)
    }
}

// MARK:- UISearchBarDelegate
extension SearchView:UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {return}
        self.endEditing(true)
        controller.didPressSearchWithText(text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.endEditing(true)
    }
}

// MARK:- ViewModels
extension SearchView {
    private func setUpViewModel() {
        controller.viewModel.loadItems = {[weak self] in
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
                self?.endEditing(true)
            }
        }
        
        controller.viewModel.loadImageAtIndex = {[weak self] image, index in
             DispatchQueue.main.async {
                guard let strongSelf = self else {return}
                let cell = strongSelf.collectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? SearchResultCollectionViewCell
                cell?.configure(image: image)
            }
        }
        
        controller.viewModel.showLoader = {
             DispatchQueue.main.async {[weak self] in
                guard let strongSelf = self else {return}
                 strongSelf.activityIndicator.frame = CGRect.init(x: strongSelf.searchBar.frame.size.width-120, y: (strongSelf.searchBar.bounds.size.height-20)/2, width: 20, height: 20)
                 strongSelf.activityIndicator.startAnimating()
            }
        }
        
        controller.viewModel.hideLoader = {[weak self] in
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
            }
        }
        
        controller.viewModel.scrollToTop = {[weak self] in
            DispatchQueue.main.async {
                self?.collectionView.setContentOffset(.zero, animated: true)
            }
        }
        
        controller.viewModel.showListError = {
            DispatchQueue.main.async {[weak self] in
                let alert = UIAlertController.init(title: "Something went wrong", message: "No imgaes found", preferredStyle: UIAlertControllerStyle.alert)
                self?.controller.present(alert, animated: true, completion: nil)
            }
        }
    }
}
