//
//  SearchController.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 02/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation
import UIKit

final class SearchController:UIViewController {
    
    public let viewModel:SearchViewModel
    private let presenter:SearchPresenter
    private let searchInteractor:SearchImagesInteractor
    private let loadImageInteractor:LoadImageInteractor
    private var searchView:SearchView!
    
    private let perPageCount:Int = 100
    
    public init(_ searchInteractor:SearchImagesInteractor, loadImageInteractor:LoadImageInteractor) {
        self.presenter = SearchPresenter()
        self.viewModel = SearchViewModel()
        self.searchInteractor = searchInteractor
        self.loadImageInteractor = loadImageInteractor
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        searchView = SearchView.init(controller: self)
        view = searchView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        searchView.viewWillTransition(to: size, with: coordinator)
    }
}

// MARK:- Actions
extension SearchController {
    @objc public func didPressSearchWithText(_ text:String) {
        let searchText = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        guard searchText.count > 0 else {return}
        viewModel.setCurrentSearchText(searchText)
        setUpSearchInteractorWith(text: searchText, page: 1)
    }
    
    @objc public func didReachEndOfScroll() {
        guard let text = viewModel.currentSearchText else {return}
        let page = viewModel.currentPage+1
        setUpSearchInteractorWith(text: text, page: page)
    }
    
    @objc public func willShowImageViewAt(_ index:IndexPath) {
        guard index.row < viewModel.items.count else {return}
        setUpLoadImageInteractor(image:viewModel.items[index.row])
    }
}

// MARK:- Interactors
extension SearchController {
    private func setUpSearchInteractorWith(text:String, page:Int) {
        let request:SearchImagesInteractor.Request = SearchImagesInteractor.Request(text: text, page: page, perPage: perPageCount)
        let completion = presenter.subscribeSearchResponse(viewModel: viewModel)
        searchInteractor.getSearchResult(request: request, completion: completion)
    }
    
    private func setUpLoadImageInteractor(image:SearchViewModel.Image) {
        let request:LoadImageInteractor.Request = LoadImageInteractor.Request(id: image.id, secret: image.secret, farm: image.farm, server: image.server)
        let completion:((LoadImageInteractor.Response)->()) = presenter.subscribeImageLoadResponse(viewModel: viewModel)
        loadImageInteractor.getImage(request: request, completion: completion)
    }
}

