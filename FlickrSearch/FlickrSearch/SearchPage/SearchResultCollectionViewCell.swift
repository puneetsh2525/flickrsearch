//
//  SearchResultCollectionViewCell.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 02/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import UIKit

class SearchResultCollectionViewCell: UICollectionViewCell {
    @IBOutlet var image:UIImageView! {
        didSet {
            image.contentMode = .scaleToFill
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    public static func name() -> String {
        return String(describing:self)
    }
    
    public func configure(image:UIImage?) {
        self.image.image = image
    }
}
