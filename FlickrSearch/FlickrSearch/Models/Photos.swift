//
//  Photos.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 01/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation

struct Photos:Codable {
    let page:Int
    let pages:Int
    let perPage:Int
    let total:String
    let photo:[Photo]
    
    struct Search:Codable {
        let photos:Photos
        let stat:String
    }
    
    private enum CodingKeys: String, CodingKey {
        case perPage = "perpage", page, pages, total, photo
    }
}

struct Photo:Codable {
    let id:String
    let secret:String
    let farm:Int
    let server:String
}


