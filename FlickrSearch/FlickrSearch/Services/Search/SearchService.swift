//
//  SearchService.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 01/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation

struct SearchService {
}

extension SearchService {
    struct Request {
        let text:String
        let page:Int
        let perPage:Int
        
        init(text:String, page:Int, perPage:Int) {
            self.text = text
            self.page = page
            self.perPage = perPage
        }
        
        init(text:String) {
            self.init(text: text, page: 1, perPage: 100)
        }
    }
}

extension SearchService {
    struct Response {
        let search:Photos.Search?
        let error:Swift.Error?
    }
}

protocol SearchImagesService {
    func getImagesWith(_ request:SearchService.Request, completion:@escaping(SearchService.Response) -> ())
}



