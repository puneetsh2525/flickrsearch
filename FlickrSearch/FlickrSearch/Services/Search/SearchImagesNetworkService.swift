//
//  SearchImagesNetworkService.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 01/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation

final class SearchImagesNetworkService:SearchImagesService {
   
    public func getImagesWith(_ request:SearchService.Request, completion:@escaping (SearchService.Response) -> ()) {
        let request = FlickrAPI.search(request).urlRequest
        URLSession.shared.dataTask(with: request) {[weak self](data, urlResponse, error) in
            guard let weakSelf = self else{return}
            weakSelf.handleResponse(data, error: error, completion: completion)
        }.resume()
    }
    
    private func handleResponse(_ data:Data?, error:Error?, completion:(SearchService.Response) -> ()) {
        guard let concreteData = data, let search = try? Photos.Search.decode(data: concreteData) else {
            completion(SearchService.Response.init(search: nil, error: error))
            return
        }
        completion(SearchService.Response.init(search: search, error: error))
    }
}
