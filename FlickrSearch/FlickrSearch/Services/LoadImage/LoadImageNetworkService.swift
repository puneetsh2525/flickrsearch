//
//  LoadImageNetworkService.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 01/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation
import UIKit

final class LoadImageNetworkService:LoadImageService {
    private let cache:ImageCache = ImageCache.shared
    
    public func getImageWith(_ request: LoadImage.Request, completion: @escaping (LoadImage.Response) -> ()) {
        cache.getImage(request.imagePath) {[weak self] image in
            self?.handleResponseFromCache(image, path:request.imagePath, completion: completion)
        }
    }
    
    private func handleResponseFromCache(_ image:UIImage?, path:String, completion: @escaping (LoadImage.Response) -> ()) {
        guard image == nil else {
            completion(LoadImage.Response.init(path:path, image: image!, error: nil))
            return
        }
        let urlRequest = URLRequest.init(url: URL.init(string: path)!)
        // Make Network call
        URLSession.shared.downloadTask(with: urlRequest) {[weak self] (url, response, error) in
            self?.handle(url: url, error: error, path:path, completion: completion)
        }.resume()
    }
    
    private func handle(url:URL?, error:Error?, path:String, completion:@escaping (LoadImage.Response) -> ()) {
        guard let url = url else {
            completion(LoadImage.Response(path:path, image: nil, error: error))
            return
        }
        guard let data = try? Data.init(contentsOf: url) else {
            completion(LoadImage.Response(path:path, image: nil, error: error))
            return
        }
        guard let image = UIImage.init(data: data) else {
            completion(LoadImage.Response(path:path, image: nil, error: error))
            return
        }
        cache.setImage(image, path: path)
        completion(LoadImage.Response(path:path, image: image, error: nil))
    }
}
