//
//  LoadImageService.swift
//  FlickrSearch
//
//  Created by Puneet Sharma2 on 01/09/18.
//  Copyright © 2018 com.puneet.sh2525. All rights reserved.
//

import Foundation
import UIKit

struct LoadImage {
}

extension LoadImage {
    struct Request {
        let imagePath:String
    }
    
    struct ImagePathRequest {
        let id:String
        let secret:String
        let farm:Int
        let server:String
    }
}

extension LoadImage {
    struct Response {
        let path:String
        let image:UIImage?
        let error:Swift.Error?
    }
}

protocol LoadImageService {
    func getImageWith(_ request:LoadImage.Request, completion:@escaping(LoadImage.Response) -> ())
}

