# FlickrSearch

![](ReadMeImages/portrait.png)

![](ReadMeImages/landscape.png)


### Architecture

This app is designed with an architecture that is heavily influenced by UncleBob's Clean [architecture](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html).

Here are a few pointers to understand code better:

1. **Interactor**
	Interactor is used to perform logic such as data fetch, persistence, etc.
	It can make use of Services/Workers to perform generic tasks.
	Interactor takes a request model and provide a response model.
	
2. **Controller**
	Controller, here, is different from MVC's Controller. Controller does not take care of view datasources/delegates, IBActions, etc.
	Here, Controller takes view actions, listens to view lifecycle and performs tasks accordingly. 
	It creates request model for interactors and passes interactors' response to presenters.
	
3. **Presenter**
 	Presenter listens to Interactors' response and updates ViewModel accordingly.
 	
4. **ViewModel**
 	ViewModel pertains to view appearance logic and gets updated by presenter.
 	
5. **View**
	Custom View class that listens to ViewModel changes and updates itself.
	Passes user interactions to Controller.
	View is supposed to be devoid of any logic.
	It can have some logic for animations and transitions. 
	
### Improvements

I have taken a lot of liberty in the code base(primarily, to complete it on time).
Here are some areas where improvements can be sought:

1. **Introduction of Coordinator and Router**
	To introduce sophisticated routing logic, in a single screen app, seemed like an overkill. But, these needs to be introduced earlier than later for multi-screen apps.
	[Coordinator](http://khanlou.com/2015/10/coordinators-redux/), design pattern provides direction to code base and can be coupled with Router and Dependency Injection frameworks, to control app flow.
	
2. **Network**
	I have used URLSession directly. It does not cater to custom headers, network logs, different response serializations, cache, etc.
	A Network layer should be added that provides all these solutions.
	
3. **Reactive Code(Rx)**
	Although Swift has closures that can help in writing reactive code but still I feel RxSwift gives us much more. It has large amount of operators and schedulers to take care of side effects.
	
4. **Texture**
	I have started using [Texture](https://texturegroup.org) to create UIs. It gives a huge boost in UI responsiveness where UI design has heavy images, complicated list designs, etc. 

5. **UIDesign**
	A good UIDesign takes creativity and creativity is not time bound. 
	

	
 
 